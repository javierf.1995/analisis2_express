const request = require("supertest");
const app = require("../app");
let server, agent;

//Se inicia el servidor para comenzar a escuchar peticiones
beforeEach(done => {
    server = app.listen(5000, err => {
        if (err) return done(err);
        agent = request.agent(server); //se obtiene el puerto en el que corre el server
        done();
    });
});
//despues de cada prueba se cierra el servidor iniciado en beforeEach
afterEach(done => {
    return server && server.close(done);
});


test('Fake Test', ()=> {
    expect(true)
})

//Server
describe("Pruebas para App/server", function() {
    test("get inicial hellow world -> /", function(done) {
        request(app)
            .get("/")
            .set("Accept", "application/json")
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
    });

    test("post /CrearUsuario nuevo", function(done) {
        request(app)
            .post("/CrearUsuario")
            .send({
                correo: "prueba@prueba.com",
                nombre: "user_test",
                passwd: "1234"
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
    });

    test("post /CrearUsuario repetido", function(done) {
        request(app)
            .post("/CrearUsuario")
            .send({
                correo: "prueba@prueba.com",
                nombre: "user_test",
                passwd: "1234"
            })
            .expect({})
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
    });

    test("post /DeleteUsuario ", function(done) {
        request(app)
            .post("/CrearUsuario")
            .send({
                correo: "prueba@prueba.com"
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
    });

    test("post /DeleteUsuario inexistente", function(done) {
        request(app)
            .post("/CrearUsuario")
            .send({
                correo: "prueba@prueba.com"
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                done();
            });
    });
});