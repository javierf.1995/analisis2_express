-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema g9
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema g9
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `g9` DEFAULT CHARACTER SET latin1 ;
USE `g9` ;

-- -----------------------------------------------------
-- Table `g9`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `g9`.`usuario` (
  `correo` VARCHAR(25) NOT NULL,
  `nombre` VARCHAR(20) NOT NULL,
  `passwd` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`correo`),
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
