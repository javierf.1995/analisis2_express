//server.js
const app = require("./app");

//listening to server 3000
app.listen(5000, () => {
    console.log("Server running on port 5000");
});