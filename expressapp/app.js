var bodyParser = require("body-parser");
const express = require("express");
const app = express();
var mysql = require("mysql");
var cors = require("cors") ;
const util = require("util");
app.use(express.json());
app.use(bodyParser.json());
app.use(cors());
var connection = mysql.createConnection({
    host: "labanalisis3.clnflalxesaa.us-east-2.rds.amazonaws.com",
    user: "admin",
    password: "labanalisis",
    database: "g9"
});
const query = util.promisify(connection.query).bind(connection);

app.post("/CrearUsuario", function(req, res) {
    var postData = req.body;
    (async() => {
        try {
            query("INSERT INTO usuario SET ?", postData, function(
                error,
                results,
                fields
            ) {
                if (error) {
                    res.end(JSON.stringify({ Error: "Error usuario repetido" }));
                }else {
                    res.sendStatus(200);
                }       
            });
        } catch (e) {
            console.log(e);           
        }
    })();
});

app.post("/DeleteUsuario", function(req, res) {
    var postData = req.body;
    // console.log(postData.correo);
    (async() => {
        try {
            /**
             * borrar usuario
             */
            query("DELETE FROM usuario WHERE correo = ? ", postData.correo, function(
                error,
                results,
                fields
            ) {
                if (error) {
                    res.end(JSON.stringify({ Error: "Error usuario inexistente" }));
                }
                res.sendStatus(200);                            
            });
        } catch (e) {
            console.log(e);
        }
    })();
});

app.post("/Iniciar", function(req, res) {
    //console.log(req.body.Correo);
    //console.log(req.body.Password);
    connection.query(
        "SELECT * FROM usuario WHERE correo =? AND passwd =?", [req.body.correo, req.body.passwd],
        function(error, results, fields) {
            if (error) {
                res.end(JSON.stringify({ Error: "Error de credenciales" }));
            }
            //console.log(JSON.stringify(results));
            res.end(JSON.stringify(results));
        }
    );
});

app.get("/", (req, res) => {
    res.status(200).send("Hello World!");
});

module.exports = app;